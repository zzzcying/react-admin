import React, { Component } from "react";   //react是默认导出对象，通常用于创建react组件，component是一个基类，用于定义类组件
import { Form, Input, Button, Row, Col } from 'antd';
import { formatMessage } from 'umi/locale';
import omit from 'omit.js';
import style from './index.less';
import ItemMap from './map';
import { getCaptchaImage } from '../../services/user';
import { setCaptchaKey } from '../../utils/authority';

const FormItem = Form.Item;

class WrapFormItem extends Component {
    static defaultProps = {       //设置默认属性，为了保证组件在没有传递特定属性时能够正常运行
        getCaptchaButtonText: 'captcha',
        getCaptchaSecondText: 'second',
    }

    constructor(props) {      //在构造函数中设置初始化状态，组件内部的数据源，用于存储可变数据
        super(props);
        this.state = {
            count: 0,
            image: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',
        }
    }

    componentWillMount() {    //组件挂载在dom前调用
        const { mode, type } = this.props;
        if (type === 'Captcha' && mode === 'image') {
            this.refreshCaptcha();
        }
    }

    componentDidMount(){    //组件挂在DOM后立即调用，进行一次性的数据和获取和DOM操作
        const {updateActive,name} = this.props;
        if(updateActive){
            updateActive(name);
        }
    }


    componentWillUnmount() {  //组件卸载
        //清除计数器
        clearInterval(this.interval);
    }


    refreshCaptcha = ()=>{
        //获取验证码
        getCaptchaImage().then(resp=>{
            const {data} = resp;
            if(data.key){
                this.setState({
                    image: data.image,
                })
                setCaptchaKey(data.key);
            }
        })
    }
    
   




}